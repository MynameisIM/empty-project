import 'promise-polyfill/src/polyfill';
import 'svgxuse';
import Imask from 'imask';
import ObjectFitImages from 'object-fit-images';
import ScrollTo from './base/script/scrollTo';
import BodyLockScroll from './base/script/body-lock-scroll';
import Form from './blocks/form/form';
import Popup from './blocks/popup/popup';
import Header from './blocks/header/header';

require('custom-event-polyfill');
require('lazysizes');
require('./autoload.scss');

ObjectFitImages(null, { watchMQ: true });

window.lazySizesConfig = window.lazySizesConfig || {};
window.lazySizesConfig.customMedia = {
  '--small': '(max-width: 767px)',
  '--medium': '(min-width: 768px) and (max-width: 1279px)',
  '--large': '(min-width: 1280px)',
};
window.lazySizesConfig.preloadAfterLoad = true;
window.lazySizesConfig.expand = 600;
window.lazySizesConfig.expFactor = 1;

window.debounce = (f, ms) => {
  let timer = null;

  return (...args) => {
    const onComplete = () => {
      f.apply(this, args);
      timer = null;
    };

    if (timer) {
      clearTimeout(timer);
    }

    timer = setTimeout(onComplete, ms);
  };
};

window.bodyLock = new BodyLockScroll();

[].slice.call(document.querySelectorAll('.header')).forEach((item) => {
  new Header(item);
});

[].slice.call(document.querySelectorAll('[name=phone]')).forEach((input) => {
  input.mask = new Imask(input, {
    mask: '+{7}(000) 000-00-00',
  });
});

[].slice.call(document.querySelectorAll('[data-anchor]')).forEach((btn) => {
  if (btn) {
    btn.addEventListener('click', (e) => {
      e.preventDefault();
      new ScrollTo(btn);
    });
  }
});

[].slice.call(document.querySelectorAll('.form')).forEach((block) => {
  new Form(block);
});

[].slice.call(document.querySelectorAll('.popup')).forEach((item) => {
  new Popup(item);
});
