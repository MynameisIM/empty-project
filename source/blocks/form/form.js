import Axios from 'axios';
import ValidateForm from '../../base/script/validateForm';

export default class Form {
  constructor(parent) {
    this.form = parent;
    const validate = new ValidateForm(this.form);

    parent.addEventListener('submit', (event) => {
      event.preventDefault();
      const result = validate.check();

      window.debounce(this.submit(result), 200);
    });
  }

  submit(result) {
    if (result !== false) {
      const data = new window.FormData(this.form);
      Axios.post(this.form.action, data)
        .then(() => {
          this.form.reset();
          Form.thanksPopup();
        });
    }
  }

  static thanksPopup() {
    const popup = document.querySelector('.popup--thanks');
    if (popup) {
      popup.classList.add('popup--opened');
    }
  }
}
