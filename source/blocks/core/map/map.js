const xhr = require('../../../static/ajax/map-marker.json');

if (window.map_marker) {
  xhr.map_marker = window.map_marker;
}

class Map {
  constructor(data) {
    this.map = data.parent.querySelector('.markets__map');
    const markets = [].slice.call(data.parent.querySelectorAll('.markets__item-wrapper'));
    this.marketList = data.parent.querySelector('.markets__list');
    this.marketsContainer = [].slice.call(data.parent.querySelectorAll('.markets__item'));
    const center = {
      lat: this.map.getAttribute('data-lat'),
      lng: this.map.getAttribute('data-lng'),
    };
    window.ymaps.ready(() => {
      const ymap = new window.ymaps.Map(this.map, {
        center: [center.lat, center.lng],
        zoom: 14,
        controls: ['zoomControl'],
      });
      ymap.behaviors.disable('scrollZoom');

      const myCollection = new window.ymaps.GeoObjectCollection({}, {
        iconLayout: 'default#image',
        iconImageHref: data.pin,
      });

      for (let i = 0; i < xhr.map_marker.length; i += 1) {
        myCollection.add(new window.ymaps.Placemark(xhr.map_marker[i], {
          iden: i,
        }));
      }
      myCollection.events
        .add('click', (e) => {
          const index = e.get('target').properties.get('iden');
          myCollection.each((el) => {
            if (el.properties.get('iden') === index) {
              el.options.set('iconImageHref', data.pinHovered);
            } else {
              el.options.set('iconImageHref', data.pin);
            }
          });
          this.removeClass();
          this.marketsContainer[index].classList.add('markets__item--current');
          this.marketList.scrollTop = this.marketsContainer[index].offsetTop;
        });

      ymap.geoObjects.add(myCollection);

      if (markets.length > 0) {
        markets.forEach((market) => {
          market.addEventListener('click', () => {
            ymap.setCenter(xhr.map_marker[Number(market.dataset.index)]);
            myCollection.each((el) => {
              if (el.properties.get('iden') === +market.dataset.index) {
                el.options.set('iconImageHref', data.pinHovered);
              } else {
                el.options.set('iconImageHref', data.pin);
              }
            });
            this.removeClass();
            market.parentNode.classList.add('markets__item--current');
          });
        });
      }
    });
  }

  removeClass() {
    if (this.marketsContainer.length > 0) {
      this.marketsContainer.forEach((market) => {
        market.classList.remove('markets__item--current');
      });
    }
  }
}

export default Map;
