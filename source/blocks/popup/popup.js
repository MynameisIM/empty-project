class Popup {
  constructor(parent) {
    this.parent = parent;
    this.closeBtns = [].slice.call(parent.querySelectorAll('.js-close'));
    this.initEvents();
  }

  removeClasses() {
    this.parent.classList.remove('popup--opened');
    this.parent.classList.remove('popup--hide');
  }

  openPopup() {
    this.parent.classList.add('popup--opened');
  }

  closePopup() {
    this.parent.classList.add('popup--hide');
    setTimeout(() => {
      this.removeClasses();
    }, 500);
  }

  initEvents() {
    if (this.closeBtns) {
      this.closeBtns.forEach((closeBtn) => {
        closeBtn.addEventListener('click', () => {
          this.closePopup();
        });
      });
    }
  }
}

export default Popup;
