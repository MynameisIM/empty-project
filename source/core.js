/* eslint-disable */
require('./autoload.scss');
const css = require('./base/fonts/fonts.scss').toString();
const head = document.head || document.getElementsByTagName('head')[0];
style = document.createElement('style');
style.type = 'text/css';
if (style.styleSheet) {
  style.styleSheet.cssText = css;
} else {
  style.appendChild(document.createTextNode(css));
}
head.appendChild(style);

var insertBefore = head.insertBefore;
head.insertBefore = function (newElement, referenceElement) {
  if (newElement.href && newElement.href.indexOf('//fonts.googleapis.com/css?family=Roboto') > -1) {
    return;
  }
  insertBefore.call(head, newElement, referenceElement);
};

function mediaqueryload(ev, isInit) {
  var queriedresource = document.querySelectorAll('.mediaquerydependent'),
    all = queriedresource.length,
    i = all,
    current = null,
    attr = null;
  if (ev && ev.type === 'resize') {
    i = all;
  }
  let changes = false;
  while (i--) {
    current = queriedresource[i];
    if (current.dataset.media &&
      window.matchMedia(current.dataset.media).matches) {
      for (attr in current.dataset) {
        if (attr !== 'media' && current.href === '') {
          current.setAttribute(attr, current.dataset[attr]);
          changes = true;
        }
      }
    }
  }
  if (changes && isInit) {
    setTimeout(() => {
      var resizeEvent = window.document.createEvent('UIEvents');
      resizeEvent.initUIEvent('resize', true, false, window, 0);
      window.dispatchEvent(resizeEvent);
    }, 200);
  }
}
mediaqueryload(null, true);
window.addEventListener('resize', mediaqueryload, false);