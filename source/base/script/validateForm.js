// import EmailValidator from 'email-validator';

export default class ValidateForm {
  constructor(form) {
    this.form = form;
    this.inputs = [].slice.call(this.form.querySelectorAll('.input__field'));
    this.regex = /(^\w.*@\w+\.\w)/;

    if (this.inputs.length !== 0) {
      this.inputs.forEach((input) => {
        input.addEventListener('keyup', () => {
          if (input.value.length !== 0) {
            input.classList.remove('input__field--error');
          }

          if (input.classList.contains('input__field--phone')) {
            if (input.value.length === 17) {
              input.classList.remove('input__field--error');
            } else {
              input.classList.add('input__field--error');
            }
          }

          if (input.classList.contains('input__field--email')) {
            if (this.regex.test(input.value)) {
              input.classList.remove('input__field--error');
            } else {
              input.classList.add('input__field--error');
            }
          }
        });
      });
    }
  }

  check() {
    let errors = 0;

    if (this.inputs.length !== 0) {
      this.inputs.forEach((input) => {
        const val = input.value;
        const required = input.classList.contains('required');
        if (required) {
          if (val.length === 0) {
            input.classList.add('input__field--error');
            errors += 1;
          } else {
            input.classList.remove('input__field--error');
          }

          if (input.classList.contains('input__field--phone')) {
            if (val.length !== 17) {
              input.classList.add('input__field--error');
              errors += 1;
            } else {
              input.classList.remove('input__field--error');
            }
          }

          if (input.classList.contains('input__field--email') && val.length !== 0) {
            if (this.regex.test(val)) {
              input.classList.remove('input__field--error');
            } else {
              errors += 1;
              input.classList.add('input__field--error');
            }
          }
        } else if (!required) {
          if (input.classList.contains('input__field--email') && val.length !== 0) {
            if (this.regex.test(val)) {
              input.classList.remove('input__field--error');
            } else {
              input.classList.add('input__field--error');
            }
          }
        }
      });
    }

    const someInputHasValue = this.inputs.some(input => input.value.length !== 0);
    let result = false;
    if (errors === 0 && someInputHasValue) {
      result = true;
    }
    return result;
  }
}
