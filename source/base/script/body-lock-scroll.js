export default class BodyLockScroll {
  constructor() {
    this.body = document.body;
    this.oldPos = null;
  }

  disable() {
    if (this.oldPos !== null) {
      this.body.style.overflow = '';
      window.scrollTo(0, this.oldPos);
      this.oldPos = null;
    }
  }

  enable() {
    if (this.oldPos === undefined || this.oldPos === null) {
      this.oldPos = window.pageYOffset;
    }
    this.body.style.overflow = 'hidden';
    this.body.scrollTop = this.oldPos;
  }
}
